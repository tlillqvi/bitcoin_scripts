#!/usr/bin/env python

import sys
import binascii as ba
import hashlib
import os
# https://github.com/bitcoin/bips/blob/master/bip-0039/english.txt
fp = open(os.path.dirname(sys.argv[0])+"/bip39_english.txt","ro")
wordlist = map(lambda s: s.strip(),fp.readlines())

hexstr = sys.stdin.readline().strip()

# Up to 8 bits of checksum
checksum = hashlib.sha256(ba.unhexlify(hexstr)).hexdigest()[:2]

bitsread = 0
data = 0 # integer buffer holding read bits
space = ""
for letter in hexstr+checksum:
    data <<= 4
    data |= int(letter,16)
    bitsread += 4
    if bitsread < 11:
        continue
    # convert most significant 11 bits to a word
    sys.stdout.write(space+wordlist[data>>(bitsread-11)])
    space = " "
    bitsread -= 11
    data &= 0x7ff >> (11-bitsread) # remove the 11 printed bits
print "\n"
