#!/usr/bin/env python

from sys import stdin,stderr,exit
import base58check

# https://en.bitcoin.it/wiki/Wallet_import_format

wif = stdin.readline().strip()

privkey = base58check.decode(wif)

if not privkey[:2] == '80':
    stderr.write("ERR: Not a mainnet address! "+privkey[:2]+"\n")
    exit(1)

is_compressed = wif[0] != '5'

if is_compressed:
    print privkey[2:-2]
else:
    print privkey[2:]
