#!/bin/bash
set -eu

which jq &>/dev/null || { echo "jq is missing"; exit 1; }

TOPDIR=$(readlink -f $(dirname $0)/..)

trap 'echo $BASH_COMMAND failed' ERR

jq '.english[]|"\(.[0]) \(.[1])"' bip39_trezor_test_vectors.json | xargs -l | while read line
do
    hex=$(set -- $line; echo $1)
    mnem=$(set -- $line; shift; echo $*)

    test -n "$hex" || break;
    test -n "$mnem" || break;

    GEN_HEX="$(echo -n "$mnem" | python $TOPDIR/mnemonic2hex.py)"
    test "$GEN_HEX" = "${hex^^}" || {
	echo "failed at $mnem got $GEN_HEX not $hex";
	exit 1;
    }
done

echo OK
