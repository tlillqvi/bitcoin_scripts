#!/bin/bash
set -eu

TOPDIR=$(readlink -f $(dirname $0)/..)

trap 'echo $BASH_COMMAND failed' ERR


# uncompressed
test 1NP6AQkH8U7pkLkJy2b9F7eEruZKrZaAPC \
     = $(echo 0471E76B6F8F1CD168881D4409169FB619B2D5FC5719140692499B4AF8C0EB30AF572E41B808A3E15CDEFC5754A89F4CDEBAF7B20FDB4648BF96A061A23B005C9B | $TOPDIR/hex2address.py ) ||
    {
	echo "FAIL";
	exit 1;
    }

# compressed
test 1KaffBtRGffXyQ2JWWE33anEzVU7W5cpUM \
     = $(echo 0371E76B6F8F1CD168881D4409169FB619B2D5FC5719140692499B4AF8C0EB30AF | $TOPDIR/hex2address.py) ||
    {
	echo "FAIL";
	exit 1;
    }

echo OK
