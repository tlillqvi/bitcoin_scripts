#!/bin/bash
set -eu

which jq &>/dev/null || { echo "jq is missing"; exit 1; }

TOPDIR=$(readlink -f $(dirname $0)/..)

trap 'echo $BASH_COMMAND failed' ERR

jq '.english[]|"\(.[0]) \(.[1])"' bip39_trezor_test_vectors.json | xargs -l | while read line
do
    hex=$(set -- $line; echo $1)
    mnem=$(set -- $line; shift; echo $*)

    test -n "$hex" || break;
    test -n "$mnem" || break;

    GEN_MNEM="$(echo -n "$hex" | python $TOPDIR/hex2mnemonic.py)"
    test "$GEN_MNEM" = "$mnem" || {
	echo "failed at $hex got $GEN_MNEM not $mnem";
	exit 1;
    }
done

echo OK
