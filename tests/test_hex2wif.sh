#!/bin/bash
set -eu

TOPDIR=$(readlink -f $(dirname $0)/..)

trap 'echo $BASH_COMMAND failed' ERR


test 5HueCGU8rMjxEXxiPuD5BDku4MkFqeZyd4dZ1jvhTVqvbTLvyTJ = $(echo 0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D | python $TOPDIR/hex2wif.py -u) || {
    echo "FAIL";
    exit 1;
}

echo OK
