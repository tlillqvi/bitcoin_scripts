#!/usr/bin/env python

import sys
import binascii as ba
import hashlib
import os
import math

# https://github.com/bitcoin/bips/blob/master/bip-0039/english.txt
fp = open(os.path.dirname(sys.argv[0])+"/bip39_english.txt","ro")
wordlist = map(lambda s: s.strip(),fp.readlines())

mnemonic = sys.stdin.readline().strip()

data=0
for word in mnemonic.split():
    data <<= 11
    data += wordlist.index(word)

cs_bits=len(mnemonic.split())/3

hexchars=math.ceil((11*len(mnemonic.split())-cs_bits)/4.0)
hexdata=("%0"+str(hexchars)+"x")%(data>>cs_bits)

hexchars=math.ceil(cs_bits/4.0)
checksum=hex(data & (0xff>>(8-cs_bits)))

calc=hashlib.sha256(ba.unhexlify(hexdata)).hexdigest()[:2] # 8 bits
calc=hex(int(calc,16)>>(8-cs_bits))
#sys.stderr.write( "\n"+calc+" "+checksum+"\n")
if calc.strip('L') != checksum.strip('L'):
    raise Exception("Checksum mismatch!")

print hexdata.upper()
