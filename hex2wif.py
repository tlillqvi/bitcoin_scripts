#!/usr/bin/env python

from sys import argv, stdin
import base58check

# https://en.bitcoin.it/wiki/Wallet_import_format

secret_hex = stdin.readline().strip()

# mainnet
secret_hex_net = '80' + secret_hex

use_compressed = not '-u' in argv
if use_compressed:
    secret_hex_net += '01'

print base58check.encode( secret_hex_net )
