#!/usr/bin/env python

# Usage:
#   stdin: 256-bit private key in hex
#  stdout: public key in hex, varying length

from ecdsa.ecdsa import generator_secp256k1, Public_key
from sys import stdin, argv, exit

# http://gobittest.appspot.com/Address
# https://github.com/bitcoinbook/bitcoinbook/blob/second_edition/code/ec-math.py
secret_hex = stdin.readline().strip()
secret_int = int(secret_hex,16)

pubkey = Public_key( generator_secp256k1,
                     generator_secp256k1 * secret_int )

# 04 for uncompressed
key_65b = '04' + '%064x' % pubkey.point.x() + '%064x' % pubkey.point.y()

# 02 or 03 for compressed
if pubkey.point.y() & 1:
    key_33b = '03' + '%064x' % pubkey.point.x()
else:
    key_33b = '02' + '%064x' % pubkey.point.x()

use_compressed = not '-u' in argv
if use_compressed:
    key = key_33b
else:
    key = key_65b

print key.upper()
