from math import log
from hashlib import sha256
from binascii import unhexlify

btc_base58alphabet="123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
alphabet = btc_base58alphabet

def _base58encode(allchunks,cnt):
    if int(cnt) < 1 and allchunks < 58:
        return alphabet[allchunks],allchunks*58
    address,topchunks = _base58encode(allchunks//58,cnt-1)
    address += alphabet[allchunks-topchunks]
    return address,allchunks*58

# base58 encoded hexdata with checksum
def encode( hexdata ):
    sha256_once = sha256(unhexlify(hexdata)).digest()
    sha256_twice = sha256(sha256_once).hexdigest()
    checksum = sha256_twice[:4*2]
    address,_ = _base58encode( int( hexdata+checksum, 16 ),
                               int( log( 16**len(hexdata+checksum), 58 )-1 ) )
    return address

def decode( wif ):
    data = 0 # integer buffer holding read bits
    for letter in wif:
        if letter not in alphabet:
            sys.stderr.write("ERR: letter %s not in alphabet!\n"%letter)
            sys.exit(1)
        data *= 58
        data += alphabet.find(letter)

    hexdata = hex(data)[2:].upper().strip('L') # chop 0x and L
    # hex is not byte aligned! Fixing that here.
    if len(hexdata)%2 == 1:
        hexdata = '0'+hexdata

    privkey=hexdata[:-8]
    checksum=hexdata[-8:]

    calc=sha256(unhexlify(privkey)).hexdigest()
    calc=sha256(unhexlify(calc)).hexdigest()[:8].upper()

    if not calc == checksum:
        raise Exception("Checksum mismatch!")

    return privkey
