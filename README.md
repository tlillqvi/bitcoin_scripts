# README #

This repo holds various Bitcoin related command-line programs.

All programs are written in Python and the goal is to keep everything small and easy to debug.

## Examples ##

### hex to mnemonic ###

    $ echo "00000000000000000000000000000000" \
    | ./hex2mnemonic.py
    abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about
    $ echo "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff" \
    | ./hex2mnemonic.py
    zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo vote

### mnemonic to hex ###

    $ echo -n "zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo zoo vote" \
    | ./mnemonic2hex.py
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

### Wallet Import Format (WIF) to bitcoin address ###

A WIF leading with a '5' indicates that the corresponding public key is uncompressed. 'K' or 'L' on the other hand indicates that the public key is compressed.

**compressed public keys**

    $ echo KwdMAjGmerYanjeui5SHS7JkmpZvVipYvB2LJGU1ZxJwYvP98617 \
    | ./wif2hex.py | ./priv2pub_key.py | ./hex2address.py
    1LoVGDgRs9hTfTNJNuXKSpywcbdvwRXpmK

**uncompressed public keys**

    $ echo 5HueCGU8rMjxEXxiPuD5BDku4MkFqeZyd4dZ1jvhTVqvbTLvyTJ \
    | ./wif2hex.py | ./priv2pub_key.py -u | ./hex2address.py
    1LoVGDgRs9hTfTNJNuXKSpywcbdvwRXpmK

### (WIF) to private key in hex format ###

    $ echo 5HueCGU8rMjxEXxiPuD5BDku4MkFqeZyd4dZ1jvhTVqvbTLvyTJ \
    | ./wif2hex.py
    0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D

### private key in hex to public key in hex ###

    $ echo 0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D \
    | ./priv2pub_key.py -u # uncompressed
    04D0DE0AAEAEFAD02B8BDC8A01A1B8B11C696BD3D66A2C5F10780D95B7DF42645CD85228A6FB29940E858E7E55842AE2BD115D1ED7CC0E82D934E929C97648CB0A

    $ echo 0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D \
    | ./priv2pub_key.py # compressed by default
    02D0DE0AAEAEFAD02B8BDC8A01A1B8B11C696BD3D66A2C5F10780D95B7DF42645C

### private key in hex to bitcoin p2pkh address ###

    $ echo 0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D \
    | ./priv2pub_key.py -u | ./hex2address.py # uncompressed
    1GAehh7TsJAHuUAeKZcXf5CnwuGuGgyX2S

    $ echo 0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D \
    | ./priv2pub_key.py | ./hex2address.py
    1LoVGDgRs9hTfTNJNuXKSpywcbdvwRXpmK
