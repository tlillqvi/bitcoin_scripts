#!/usr/bin/env python

# Usage:
#   stdin: public key in hex
#  stdout: p2pkh bitcoin address

from ecdsa.ecdsa import generator_secp256k1, Public_key
from sys import stdin, argv, exit
from binascii import unhexlify
from hashlib import sha256, new
import base58check

# http://gobittest.appspot.com/Address
# https://github.com/bitcoinbook/bitcoinbook/blob/second_edition/code/ec-math.py
key = stdin.readline().strip()

sha256_1st = sha256(unhexlify(key)).hexdigest()
if '-d' in argv: print "SHA256:",sha256_1st.upper()

h = new('ripemd160') # from openssl
h.update(unhexlify(sha256_1st))
ripemd = h.hexdigest()
if '-d' in argv: print "RIPEMD160",ripemd.upper()

net_ripemd = '00'+ripemd # network 00

p2pkh = base58check.encode( net_ripemd )

print p2pkh
